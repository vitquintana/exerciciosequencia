/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    

    @FXML
    private TextField nomeText;

    @FXML
    private TextField senhaText;

    @FXML
    private TextField confirmaText;

    @FXML
    private TextArea interessesArea;

    @FXML
    private Label label;

    @FXML
    private Button idCadastro;

    @FXML
    void cadastrar(ActionEvent event) {
        Usuario u= new Usuario();
       
       u.setNome(nomeText.getText());
       u.setSenha(senhaText.getText());
       u.inserirUsuario();
       
      String[] partes = interessesArea.getText().split(",") ;
  
       
        for(int x=0;x<partes.length;x++){
            Interesses i= new Interesses();
            i.setUsuario(u);
            i.setNome(partes[x]);
            i.inserirInteresse();
        }
    }




    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
