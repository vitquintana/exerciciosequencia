/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Vitória Quintana
 */
public class Interesses {
    private String nome;
    private Usuario usuario;
    private int idInteresse;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
   

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdInteresse() {
        return idInteresse;
    }

    public void setIdInteresse(int idInteresse) {
        this.idInteresse = idInteresse;
    }
    
     public int inserirInteresse() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
           PreparedStatement ps = null;
        Statement st=null;
        String insertTableSQL = "INSERT INTO OO_Interesse"
                + "(nome,id_Usuario,id_Interesse) VALUES"
                + "(?,?,seq_Interesse.Nextval)";

       try {

            String generatedColumns[] = {"id_Interesse"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, this.getNome());
            ps.setInt(2,usuario.getIdUsuario());
         

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setIdInteresse(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.idInteresse;
     }
}


